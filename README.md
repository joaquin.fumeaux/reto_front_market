# Urumarket front
Proyecto desarrollado con Angular 15.


## Observaciones
1. No se pueden agregar, modificar ni eliminar productos desde el front, 
así que se tienen que agregar algunos productos desde el back.
2. No se pudo implementar un Login a tiempo, por lo cual se obtiene un usuario creado desde el back.
Para ello se debe crear un usuario con documento `48485074`, así también se podrá ver el historial de compras del usuario.
3. Para correr el proyecto se utiliza el comando `ng serve -o`. Se debe tener el back también corriendo.
