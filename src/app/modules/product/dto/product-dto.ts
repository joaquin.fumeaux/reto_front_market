export interface ProductDto{
price: any;
quantity: any;
imageUrl: any;
  id: number,
  name: string,
  description: string,
  inInventory: number,
  enabled: boolean,
  min: number,
  max: number
}
