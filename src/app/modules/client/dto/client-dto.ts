export interface ClientDto{
	documentId: string,
  documentType: string,
  name: string,
  surname: string,
  phoneNumber: string,
  address: string
}
