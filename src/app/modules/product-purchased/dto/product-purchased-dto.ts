import { ProductDto } from "src/app/modules/product/dto/product-dto";

export interface ProductPurchasedDto{
  id?: number,
  productId: number,
  quantity: number,
  product: ProductDto
}
